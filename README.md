﻿﻿# MisterGames Scenes v0.1.0

## Usage
- todo

## Assembly definitions
- MisterGames.Scenes
- MisterGames.Scenes.Editor

## Dependencies
- MisterGames.Common
- MisterGames.Common.Editor

## Installation
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common/) package
- Top menu MisterGames -> Packages, add packages: 
  - [Scenes](https://gitlab.com/theverymistergames/scenes/)